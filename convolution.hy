#!/home/lsusr/.local/bin/hy
(require [meta.place [fnp]]
         [utils.utils [swich]])

(setv _lazies {})

(defclass Independent []
  (defn --init-- [self symbol]
    (setv self.symbol symbol))
  (defn --repr-- [self]
    (+ "Independent " self.symbol)))

(defclass Dependent []
  (defn --init-- [self symbol function]
    (setv self.symbol symbol
          self.function function
          self.params function.params))
  (defn --repr-- [self]
    (+ "Dependent " self.symbol)))

(defmacro seti [symbol]
  `(do
     (setv ~symbol (Independent '~symbol))
     (setv (. _lazies ['~symbol]) ~symbol)))

(defmacro setd [symbol function]
  `(do
     (setv ~symbol (Dependent '~symbol ~function))
     (setv (. _lazies ['~symbol]) ~symbol)))

(defn get-symbols [dependent]
  )

(defn getd [symbol params]
  ;; TODO implement
  )

(seti x)
(setd y (fnp [x] (* 2 x)))
(setd z (fnp [y] (inc y)))

"
;; TODO create switch statement
(defn get-dependencies [dependent]
  ;; TODO finish
  (lfor param dependent.params
  (for [param dependent.params]
    (print param))))

(print (get-dependencies z))
"

(print {x 3})
;(getd y {x 3})
