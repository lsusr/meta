# Proper Places
A proper place is uniquely determined by its value. For example, the place of the integer 1 might be './.places/int/1'.
A proper place is uniquely specified by its value.
# Trapdoor Places
Some datatypes are too large or variable to make hashing them practical. These places are determined by the code which determined them but their place cannot be determined by the value.
