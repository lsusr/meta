#!/home/lsusr/.local/bin/hy
(require [meta.place [defnp $
                      get-globals-intersection-access]])

(setv core 1)
(defnp foo [] core)
(defnp zoo [] #$ (foo))
(assert (= 1 #$ (zoo)))

(setv core 2)
(assert (= 2 #$ (zoo)) (+ "error zoo is " (str #$ (zoo))))
