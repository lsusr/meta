#!/home/lsusr/.local/bin/hy
(defclass Unpickleable []
  (defn --init-- [self identifier]
    (setv self.identifier identifier)
    (setv self.function (fn []))))

(defn load-unpickleable [filepath]
  (;; TODO Read identifier from file.
   ;; TODO Create new Unpickleable with the identifier.
   #_"TODO @hath implement")

(defn save-unpickleable [filepath]
  ;; TODO Write identifier to file, as plaintext.
  ;; TODO Write this function manually. Don't use pickle.
   #_"TODO @hath implement")

(import pickle)
(with [f (open "temp.pickle" "wb")]
  (pickle.dump (Unpickleable "asdf") f))
