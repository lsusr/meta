#!/home/lsusr/.local/bin/hy

;; Lazy
(import [lazy [getd]])
(require [lazy [seti setd]])
(seti x)
(setd y (* x x))

(print y.function.identifier)
