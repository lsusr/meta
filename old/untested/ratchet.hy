#!/home/lsusr/.local/bin/hy

(import [place [to-place]])
(require [place [$ ^ fnp defnp]])
(import [lazy [getd]])
(require [lazy [seti setd]])

(import [utils.utils [directory]])
(import os pickle)
(setv +search+ (directory (os.path.join (first (os.path.split --file--)) ".search")))

(defclass GridRange []
  (defn --init-- [self begin end]
    (setv self.begin begin
          self.end end))
  (defn --iter-- [self]
    (for [i (range self.begin self.end)]
      (yield i))))

(defn grid-search [lazy ranges]
  (assert (= (set (.keys ranges))
             lazy.root-dependencies))
  (setv search-dir (directory (os.path.join +search+ lazy.identifier)))
  (setv minimum-place (os.path.join search-dir "minimum"))
  (setv previous-minimum
        (if (os.path.exists minimum-place)
            (with [f (open place "rb")]
              (pickle.load f))
            None))
  (setv sorted-keys (sorted (.keys ranges)))
  (for [values (product #*(map (fn [key]
                                 (setv value (. ranges [key]))
                                 (if (isinstance value GridRange)
                                     value
                                     [value]))
                               sorted-keys))]
    (setv parameter-dict (dict (zip sorted-keys (map to-place values))))
    (setv place
          (getd lazy parameter-dict))
    (setv value #$ place)
    (if (or (= previous-minimum None)
            (< value previous-minimum))
        (do
          (if (os.path.exists minimum-place)
              (os.remove minimum-place))
          (os.link place minimum-place)
          (with [f (open (os.path.join search-dir "minimum-parameters") "wb")]
                 (pickle.dump
                   parameter-dict f))
          (setv previous-minimum value)))))

(defn minimum [lazy]
  (setv search-dir (directory (os.path.join +search+ lazy.identifier)))
  (setv minimum-place (os.path.join search-dir "minimum"))
  (assert (os.path.exists minimum-place)
          (+ "You have not yet done a search for " (str lazy)))
  minimum-place)

