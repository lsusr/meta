#!/home/lsusr/.local/bin/hy
(require [utils.unit_tests [unit-test]])
(import [utils.unit_tests [run-unit-tests]])

(require [meta.place [fnp defnp $ ^]])

(unit-test place-placeable-values
           (assert (.endswith #^ 1 ".places/int/1.pickle"))
           (assert (.endswith #^ 2 ".places/int/2.pickle")))

(unit-test places-can-be-unplaced
           (assert (= #$ #^ 1 1))
           (assert (= #$ #^ 2 2)))

(unit-test place-based-anonymous=functions-without-arguments
           (setv one (fnp [] 1))
           (assert (= 1 #$ (one)))
           (setv two (fnp [] 2))
           (assert (= 2 #$ (two))))

(unit-test place-based-function-without-arguments
           (defnp one [] 1)
           (assert (= 1 #$ (one)))
           (defnp two [] 2)
           (assert (= 2 #$ (two))))

(unit-test place-based-functions-wth-parameters
           (defnp ident [x] x)
           (assert (= 1 #$ (ident #^ 1)))
           (assert (= 2 #$ (ident #^ 2)))
           (defnp plus [x y] (+ x y))
           (assert (= 2 #$ (plus #^ 1 #^ 1)))
           (assert (= 5 #$ (plus #^ 2 #^ 3)))
           (defnp times [x y] (* x y))
           (assert (= 1 #$ (times #^ 1 #^ 1)))
           (assert (= 6 #$ (times #^ 2 #^ 3))))

(unit-test place-based-functions-accessing-external-values
           (setv glob 3)
           (defnp double-glob [] (* 2 glob))
           (assert (= 6 #$ (double-glob))
                   "double-glob failed to read 3")
           (setv glob 4)
           (assert (= 8 #$ (double-glob)) "double-glob did not use used new var 4")
           (defnp double-glob [] (* 2 glob))
           (assert (= 8 #$ (double-glob)) ;; fails
                   "redefined double-glob fails to use new value"))

(unit-test functions-acting-on-other-functions
           (defnp foo [] 1)
           (defnp soo [] #$ (foo))
           (assert (= 1 #$ (soo)) "initial soo fails")
           (defnp foo [] 2)
           (assert (= 2 #$ (foo)) "redefined foo fails")
           ;(print "foo diagnostic string is" foo.identifier-string)
           ;(print)
           ;(print "foo globals used is" foo.globals-used)
           ;(print)
           ;(print "foo globals-available used is" foo.globals-available)
           ;(print)
           ;(print "foo body-symbols used is" foo.body-symbols)
           ;(print)
           (defnp soo [] #$ (foo))
           ;(print "soo diagnostic string is" soo.identifier-string)
           ;(print)
           ;(print "soo globals used is" soo.globals-used)
           ;(print)
           ;(print "soo globals-available used is" soo.globals-available)
           ;(print)
           ;(print "soo body-symbols used is" soo.body-symbols)
           ;(print)
           ;(print "soo locals available" soo.locals-available)
           #_(assert (= 2 #$ (soo)) ;; fails
                   "soo with redefined foo fails"))

(run-unit-tests)
