#!/home/lsusr/.local/bin/hy
(import [utils.utils [directory]])
(import [hashlib [md5]]
        [hy.models [HySymbol]]
        os pickle)

(setv +places+ (directory (os.path.join (first (os.path.split --file--))
                                        ".places")))

(defn to-place [value]
  "Turns simple datatypes into places"
  (import os pickle
          [meta.place [+places+]])
  (setv place
        (if (= (type value) int)
            (os.path.join
              (directory (os.path.join +places+ "int"))
              (+ (str value) ".pickle"))
            (= (type value) str)
            (os.path.join
              (directory (os.path.join +places+ "str"))
              (+ (.hexdigest (md5 (.encode value))) ".pickle"))
            (= value None)
            (os.path.join +places+ "None")
            (raise (NotImplementedError
                     (+ "Cannot place " (str value)
                        " of type " (str (type value)))))))
  (if-not (os.path.exists place)
          (with [f (open place "wb")]
            (pickle.dump value f)))
  place)

(deftag ^ [expr] `(do (import [meta.place [to-place]]) (to-place ~expr)))

(defn from-place [place]
  (import os)
  (assert (= (type place) str)
          f"{place} is not a place")
  (assert (os.path.exists place)
          f"place {place} does not exist")
  (with [f (open place "rb")]
    (pickle.load f)))

(deftag $ [expr] `(do (import [meta.place [from-place]]) (from-place ~expr)))

;; What dependencies do we have to handle?
;; - parameters                            [critical] [done]
;; - the function's own code               [critical] [done]
;; - global variables & external functions [important] [done]
;; - imported functions                    [low priority]
;; - Hy language functions                 [very unimportant]

(defmacro/g! fnp [params &rest body]
  "Defines a place-based function"
  "Warning: This function accepts only ordered parameters."
  (setv body-symbols (set (flatten body)))
  `(do
     (import os pickle types
             [meta.place [+places+]])
     (setv ~g!globals-used
           (- (.intersection
                (set (map HySymbol (.keys (globals))))
                '~body-symbols)
              (set '~params)))

     (setv ~g!identifier-string
           (.join "//"
                  (+ [(str '~body)] #_"code hash"
                     (lfor ex (map eval (sorted ~g!globals-used))
                           (if (isinstance ex types.FunctionType)
                               (if (hasattr ex "identifier")
                                   (+ "function" ex.identifier)
                                   (= ex.--module-- "hy.core.language")
                                   (+ "function" (str ex) "hy.core.language")
                                   (raise
                                     (TypeError
                                       (+ "Non-identifiable function " (str ex)))))
                               (to-place ex))))))
     (import [hashlib [md5]])
     (import [meta.place [from-place]])
     (setv ~g!identifier (.hexdigest (md5 (.encode ~g!identifier-string))))

     (setv ~g!output
           (fn ~params
             (setv ~g!cache-path
                   (os.path.join
                     +places+
                     (+ ~g!identifier
                        "-"
                        (.hexdigest (md5 (.encode (str ~params)))))))
             (if-not (os.path.exists ~g!cache-path)
                     (do
                       (setv ~g!output-val
                             ((fn ~params ~@body)
                               #*(map from-place ~params)))
                       (with [f (open ~g!cache-path "wb")]
                         (pickle.dump ~g!output-val f))))
             ~g!cache-path))

     (setv (. ~g!output identifier) ~g!identifier)

     ~g!output))

(defmacro defnp [symbol params &rest body]
  `(setv ~symbol (fnp ~params ~@body)))
