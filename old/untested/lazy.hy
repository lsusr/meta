#!/home/lsusr/.local/bin/hy
(require [hy.extra.anaphoric [ap-if]])
(import [hashlib [md5]])

(defclass Independent []
  (defn --init-- [self symbol]
    (setv self.symbol symbol))
  (defn --repr-- [self]
    (+ "Independent " self.symbol))
  (defn --lt-- [self other]
    (< (repr self) (repr other)))
  (defn _get-all-dependencies [self]
    (set [self])))

(defclass Dependent []
  (defn --init-- [self symbol direct-dependencies root-dependencies function]
    (setv self.symbol symbol
          self.direct-dependencies direct-dependencies
          self.root-dependencies root-dependencies
          self.function function)
    (setv self.identifier (._get-identifier self)))
  (defn --repr-- [self]
    (+ "Lazy " self.symbol))
  (defn --lt-- [self other]
    (< (repr self) (repr other)))
  (defn _get-all-dependencies [self]
    (.union (set [self])
            (set (flatten (map (fn [dep] (dep._get-all-dependencies))
                               self.direct-dependencies)))))
  (defn _get-identifier [self]
    (.hexdigest
      (md5 (.encode
             (str (list (map (fn [dependency]
                               (if (isinstance dependency Independent)
                                   (str dependency)
                                   dependency.function.identifier))
                             (sorted (._get-all-dependencies self))))))))))

(setv _dependencies {})

(defmacro seti [symbol]
  (assert (not-in symbol _dependencies))
  (setv (. _dependencies [symbol]) None)
  `(do
     (import [lazy [Independent]])
     (setv ~symbol (Independent '~symbol))))

(defn _root-dependencies-from-direct [direct-dependencies]
  (setv output (set))
  (for [dependency direct-dependencies]
    (ap-if (. _dependencies [dependency])
           (.update output (_root_dependencies-from-direct it))
           (.add output dependency)))
  output)

(defmacro setd [symbol code]
  (assert (not-in symbol _dependencies))
  (setv direct-dependencies
        (.intersection (set (.keys _dependencies))
                       (set (flatten code))))
  (setv (. _dependencies [symbol]) direct-dependencies)
  (setv root-dependencies
        (_root-dependencies-from-direct direct-dependencies))
  `(do
     (require [place [fnp]])
     (import [lazy [Dependent]])
     (setv ~symbol
           (Dependent
             '~symbol
             ~direct-dependencies
             ~(_root-dependencies-from-direct direct-dependencies)
             (fnp ~(sorted direct-dependencies) ~code)))))

(defn getd [dependent independents]
  (assert (= (.keys independents)
             (. dependent root-dependencies))
          "independents does not match root-dependencies")
  ((. dependent function)
    #**(dfor
         direct-dependency
         dependent.direct-dependencies
         [direct-dependency.symbol
          (if (isinstance direct-dependency Independent)
              (. independents [direct-dependency])
              (getd direct-dependency
                    (dfor
                      [independent value]
                      (.items independents)
                      :if (in independent (. direct-dependency root-dependencies))
                      [independent value])))])))
