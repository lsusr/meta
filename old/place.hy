#!/home/lsusr/.local/bin/hy

(import [utils.utils [directory +CR+]])
(import [hashlib [md5]]
        [hy.models [HySymbol]]
        os pickle)

(setv +places+ (directory (os.path.join
                            (first (os.path.split --file--))
                            ".places")))

(defn to-place [value]
  "converts values into places"
  (import os pickle)
  (setv place
        (if (= (type value) int)
          (os.path.join
            (directory (os.path.join +places+ "int"))
            (+ (str value) ".pickle"))
          (= (type value) str)
          (os.path.join
            (directory (os.path.join +places+ "str"))
            (+ (.hexdigest (md5 (.encode value))) ".pickle"))
          (= value None)
          (os.path.join +places+ "None")
          (raise (NotImplementedError
                   (+ "Cannot place " (str value)
                      " of type " (str (type value)))))))
  (if-not (os.path.exists place)
    (with [f (open place "wb")]
      (pickle.dump value f)))
  place)

(deftag ^ [expr] `(do (import [meta.place [to-place]])
                      (to-place ~expr)))

(defn from-place [place]
  (import os)
  (assert (= (type place) str)
          f"{place} is not a place")
  (assert (os.path.exists place)
          f"place {place} does not exist")
  (with [f (open place "rb")]
    (pickle.load f)))

(deftag $ [expr] `(do (import [meta.place [from-place]])
                      (from-place ~expr)))

(defn place? [place]
  "indicates whether path is a place"
  (and (isinstance place string)
       (.startswith place +places+)
       (os.path.exists place)))

(import [utils.utils [directory]])
(setv +place-root+ (directory "/home/lsusr/code/meta/.place/"))

"Macro creates temporary filesystem"
(defmacro/g! deff [symbol params &rest code]
             (import [hashlib [md5]])
             (setv code-hash (.hexdigest md5 (.encode (str '~code))))
             `(do
                (defn ~symbol ~params
                  (for [~g!param ~params]
                    (assert (place? ~g!param)
                            "attempted to pass non-place "
                            (repr ~g!param)
                            " to deff " (repr '~symbol)))
                  ;; TODO continue here
                  (do
                    ~@code))
                (setv (. ~symbol code-hash) ~code-hash)))
