#!/usr/local/bin/hy
(import utils.utils [directory +CR+ first])
(require utils.utils [my-with])
(import hashlib [md5]
        datetime [datetime]
        hy.models [Symbol]
        keras os pickle)
(import hy.pyops [*])
(require hyrule [defmacro/g! ->])

(setv +places-root-name+ (directory "/home/lsusr/code/meta/.places"))
(setv +places+ (directory (os.path.join
                            (first (os.path.split __file__))
                            +places-root-name+)))

(defn to-place [value]
  "converts values into places"
  (setv place
        (cond
          [(= (type value) int)
           (os.path.join
             (directory (os.path.join +places+ "int"))
             (+ (str value) ".pickle"))]
          [(= (type value) float)
           (os.path.join
             (directory (os.path.join +places+ "float"))
             (+ (str value) ".pickle"))]
          [(= (type value) bool)
           (os.path.join (directory (os.path.join +places+ "bool")) (str value))]
          [(= (type value) type)
           (os.path.join (directory (os.path.join +places+ "type")) (str value))]
          [(= (type value) str)
           (os.path.join
             (directory (os.path.join +places+ "str"))
             (+ (.hexdigest (md5 (.encode value))) ".pickle"))]
          [(= (type value) tuple)
           (os.path.join
             (directory (os.path.join +places+ "tuple"))
             (+ (str value) ".pickle"))]
          [(= (type value) list)
           (os.path.join
             (directory (os.path.join +places+ "tuple"))
             (+ (.hexdigest (md5 (.encode (str value)))) ".pickle"))]
          [(and (hasattr value "__module__")
                ;; Warning. This doesn't factor in changes to version.
                ;; If your version changes then you must refresh the cache.
                (in value.__module__ allowed-modules))
           (os.path.join (directory (os.path.join +places+ value.__module__))
                         value.__name__)]
          [(and (= (str (type value)) "<class 'module'>")
                (in value.__name__ allowed-modules))
           (os.path.join (directory (os.path.join +places+ "module2"))
                         value.__name__)]
          [(is value None)
           (os.path.join +places+ "None")]
          [True
           (raise (NotImplementedError
                    (+ "Cannot place " (str value)
                       " of type " (str (type value))
                       (if (hasattr value "__module__")
                         (+ " and module " value.__module__)
                         ""))))]))
  (if (not (os.path.exists place))
    (do ;; todo use with
        (setv f (open place "wb"))
        (pickle.dump value f)
        (.close f)))
  place)

(defmacro "#^" [expr] `(to-place ~expr))

(defn from-place [place]
  (import os)
  (assert (= (type place) str)
          f"{place} is not a place")
  (assert (os.path.exists place)
          f"place {place} does not exist")
  (cond
    [(.endswith place ".png")
     (do
       (import PIL [Image])
       (Image.open place))]
    [(.endswith place ".keras")
     (tuple [(keras.models.load-model place :compile False)
             (my-with [f (open (+ place ".history") "rb")]
                      (pickle.load f))])]
    [(.endswith place ".pickle")
     (my-with [f (open place "rb")]
              (pickle.load f))]
    [True (NotImplementedError
            (+ "Cannot unplace " place))]))

(defmacro "#$" [expr] `(from-place ~expr))

(defn place? [place]
  "indicates whether path is a place"
  (and (isinstance place str)
       (in +places-root-name+ place)
       (os.path.exists place)))

(setv allowed-functions
      (set ['targ ;; todo: use constant instead
            'dispatch-tag-macro
            ]))

(setv allowed-types
      (set [int]))

(setv allowed-modules
      (set ["collections"
            "hy.core.language"
            "hy.core.shadow"
            "hyrule.misc"
            "meta.place"
            "multiprocessing.context"
            "pathlib.Path"
            "pandas.io.parsers.readers"
            "sklearn.linear_model._base"
            "tensorflow"
            "tqdm"
            "builtins"]))

(defmacro/g! fnp [params #* code]
             `(base-fnp "pickle" ~params ~@code))

(defmacro/g! fnf [params #* code]
             `(base-fnp "directory" ~params ~@code))

(defmacro/g! fnk [params #* code]
             `(base-fnp "keras" ~params ~@code))

(defmacro/g! fni [params #* code]
             `(base-fnp "image" ~params ~@code))

(defn _get-globals-intersection [globals-dict code-symbols-set]
  (.intersection
    (set (map (fn [key] (Symbol (-> key
                                      (.replace "_" "-")
                                      (.replace "hyx-Xplus-signX" "+")
                                      (.replace "Xplus-signX" "+"))))
              (.keys globals-dict)))
    code-symbols-set))

(defmacro/g! get-globals-intersection-of-function [place-based-function]
             `(do
                (import meta.place [_get-globals-intersection])
                (_get-globals-intersection (globals)
                                           (. ~place-based-function code-symbols))))

(defmacro/g! get-globals-intersection-of-set [symbols-to-intersect-with-globals]
             `(do (import meta.place [_get-globals-intersection])
                  (_get-globals-intersection (globals)
                                             ~symbols-to-intersect-with-globals)))

(defn fnp? [obj]
  "Tests whether an object is a place-based function"
  (hasattr obj "code_symbols"))

(defmacro/g! get-globals-intersection-recursive [to-search searched]
             `(do
                (require meta.place [get-globals-intersection-of-function])
                (import meta.place [fnp?]
                        utils.utils [first])
                (setv ~g!to-search ~to-search
                      ~g!searched ~searched)
                (while (- ~g!to-search ~g!searched)
                  (setv ~g!searching (first (- ~g!to-search ~g!searched)))
                  (if (not (or (.startswith (str ~g!searching) ".")
                              (= ~g!searching 'targ))) ;; TOOD use constant in code
                    (try
                      (.add ~g!searched ~g!searching)
                      (import hy [eval :as hy-eval])
                      (setv ~g!evalled (hy-eval ~g!searching))
                      (if (fnp? ~g!evalled)
                          (setv ~g!to-search
                                (.union ~g!to-search
                                        (get-globals-intersection-of-function
                                          ~g!evalled))))
                      (except [NameError])))
                  (.add ~g!searched ~g!searching))
                ~g!searched))

(defmacro/g! get-globals-intersection-access [place-based-function]
             #_"Used for debugging"
             `(get-globals-intersection-recursive
                (. ~place-based-function code-symbols) (set)))

(defmacro/g! base-fnp [kind params #* code]
             ;; assert fails for reason I don't understand
             #_(assert (in kind
                           (set ["pickle" "directory" "keras" "image"]))
                       f"kind {kind} is not valid")
             `(do
                (import os pickle random
                        datetime [datetime]
                        hashlib [md5]
                        meta.place [is-place to-place from-place
                                     +places+ allowed-modules allowed-types]
                        utils.utils [directory])
                (require meta.place [get-globals-intersection-recursive
                                      get-globals-intersection-of-set])
                #_"TODO ignore globals if they intersect params"
                (defn place-function? [value]
                  (and (callable value)
                       (hasattr value "identifier")))
                (setv ~g!hashed-code (.hexdigest (md5 (.encode (str '~code)))))
                (setv ~g!identifier ~g!hashed-code)
                (setv ~g!function
                      (fn ~params
                        (lfor ~g!param ~params
                              (assert (place? ~g!param)
                                      (+ "Passed non-place "
                                         (str ~g!param)
                                         " to place-based function")))
                        (import hyrule [flatten])
                        (setv ~g!ggir-output
                              (get-globals-intersection-recursive
                                (set (flatten '~code))
                                (set)))
                        (setv ~g!globals-intersection-for-now
                              (set 
                                (filter 
                                  (fn [symbol]
                                    "used to be called \"ignorable-function-filter\""
                                    "Returns False for objects in allowed-modules"
                                    (try
                                      (if (in symbol allowed-functions)
                                        (return False))
                                      (if (.startswith (str symbol) ".")
                                        (return False))
                                      (import hy [eval :as hy-eval])
                                      (setv evalled (hy-eval symbol))
                                      (not (and (hasattr evalled "__module__")
                                                (in evalled.__module__ allowed-modules)))
                                      (except [NameError] True)))
                                  ~g!ggir-output)))
                        (setv ~g!globals-intersection-for-now
                              (set (filter
                                     (fn [symbol]
                                       (not-in symbol '~params))
                                     ~g!globals-intersection-for-now)))
                        (setv ~g!globals-intersection-for-now
                              (get-globals-intersection-of-set
                                ~g!globals-intersection-for-now))
                        (setv ~g!hashed-globals
                              (.hexdigest
                                (md5
                                  (.encode
                                    (.join
                                      "☆spacer★"
                                      (lfor
                                        ~g!global-symbol
                                        (sorted ~g!globals-intersection-for-now
                                                :key (fn [item] (tuple
                                                                  [(str (type item))
                                                                   item])))
                                        (+ (str ~g!global-symbol)
                                           "☆spacer2★"
                                           (do
                                             (import hy [eval :as hy-eval])
                                             (setv ~g!evalled
                                                   (hy-eval ~g!global-symbol))
                                             (if (place-function? ~g!evalled)
                                               (. ~g!evalled identifier)
                                               (to-place ~g!evalled))))))))))
                        (setv ~g!hashed-params
                              (.hexdigest
                                (md5 (.encode
                                       (.join "☆spacer★" ~(sorted params))))))
                        (setv ~g!output-path
                              (+ (directory
                                   (os.path.join
                                     +places+
                                     (cond
                                       [(= ~kind "directory")
                                       "fnf" ]
                                       [(= ~kind "pickle")
                                       "fnp"]
                                       [(= ~kind "keras")
                                       "fnk"]
                                       [(= ~kind "image")
                                       "fni"]
                                       [True (raise (NotImplementedError
                                                "kind value of "
                                                ~kind))])))
                                 ~g!identifier "-"
                                 ~g!hashed-globals "-"
                                 ~g!hashed-params
                                 (cond
                                   [(= ~kind "directory")
                                   "/" ]
                                   [(= ~kind "pickle")
                                   ".pickle"]
                                   [(= ~kind "keras")
                                   ".keras"]
                                   [(= ~kind "image")
                                   ".png"]
                                   [(raise (NotImplementedError
                                            "kind value of "
                                            ~kind))])))
                        (if (not (os.path.exists ~g!output-path))
                          (do
                            ~@(lfor param (sorted params)
                                    `(setv ~param (from-place ~param)))
                            (cond
                              [(= ~kind "directory")
                               (do
                                 (setv ~g!tmp-filename
                                       (+ (directory (os.path.join +places+
                                                                   "tmp/"))
                                          (.isoformat (datetime.now))
                                          (str (random.random)) "/"))
                                 (os.mkdir ~g!tmp-filename)
                                 (setv targ ~g!tmp-filename)
                                 ~@code
                                 (os.rename ~g!tmp-filename ~g!output-path))]
                              [(= ~kind "pickle")
                               (do
                                 (setv ~g!output-value (do ~@code))
                                 (setv ~g!file (open ~g!output-path "wb"))
                                 (pickle.dump ~g!output-value ~g!file)
                                 (.close ~g!file)
                                 )]
                              [(= ~kind "keras")
                               (do
                                 (setv [~g!model ~g!history] (do ~@code))
                                 ;; save keras model
                                 (.save ~g!model ~g!output-path)
                                 ;; save keras history
                                 (setv ~g!file (open (+ ~g!output-path ".history") "wb"))
                                 (pickle.dump ~g!history ~g!file)
                                 (.close ~g!file))]
                              [(= ~kind "image")
                               (do
                                 (setv ~g!output-value (do ~@code))
                                 (.save ~g!output-value ~g!output-path))]
                              [True (raise (NotImplementedError
                                             (+ "kind value of " ~kind)))])))
                        ~g!output-path))
(import hyrule [flatten])
(setv (. ~g!function identifier)
      ~g!identifier
      (. ~g!function code-symbols)
      (set (flatten '~code))
      (. ~g!function params)
      (set '~params))
~g!function))

(defmacro defnp [symbol params #* code]
  `(do
     (require meta.place [fnp])
     (setv ~symbol (fnp ~params ~@code))))

(defmacro defnf [symbol params #* code]
  `(do
     (require meta.place [fnf])
     (setv ~symbol (fnf ~params ~@code))))

(defmacro defnk [symbol params #* code]
  `(do
     (require meta.place [fnk])
     (setv ~symbol (fnk ~params ~@code))))

(defmacro defni [symbol params #* code]
  `(do
     (require meta.place [fni])
     (setv ~symbol (fni ~params ~@code))))

;; I should instead create a dependency graph datastructure first
;; and then construct the graph from my dependency datastructure.
;; That way I can have more customizable flowcharts.
(defmacro/g! show-flowchart []
  `(do
     (setv +show-root-hyperparameters+ False)
     (import meta.place [fnp?])
     (require meta.place [get-globals-intersection-of-set])
     (setv globals-copy (.copy (globals)))
     (import graphviz)
     (setv g (graphviz.Digraph "G" :filename ".temporary/flowchart.gv"))
     (setv place-based-function-symbols
           (sfor [key val] (.items globals-copy)
                 :if (fnp? val)
                 key))

     ;;Create Nodes
     (for [[key val] (.items globals-copy)]
       (if (and (fnp? val)
                (not (or (.startswith key "_hyx_function")
                         (.startswith key "_hy_anon_var"))))
         (if val.params
           (g.node (.replace key "_" "-") :shape "cds"))))

     (for [[key val] (.items globals-copy)]
       (if (and (fnp? val)
                (not (or (.startswith key "_hyx_function")
                         (.startswith key "_hy_anon_var"))))
         (for [dependency (get-globals-intersection-of-set val.code-symbols)]
           (setv ~g!is-hyperparameter (not (in (.replace (str dependency) "-" "_")
                                               place-based-function-symbols)))
           (if (and ~g!is-hyperparameter
                    +show-root-hyperparameters+)
             (g.node (.replace (str dependency) "_" "-")
                     :shape "rectangle"
                     :style "filled"
                     :color "lightgrey"))
           (if (or (not ~g!is-hyperparameter)
                   +show-root-hyperparameters+)
             (g.edge dependency (.replace key "_" "-"))))))
     (g.view)))

;; Tests

#_"test get-globals-intersection"
#_(setv three 3) #_"y is deliberately not set"
#_(assert (= (_get-globals-intersection '(three y))
           (set ['three])))

#_"test fnp"
;(setv fun0 (fnp [p] p)) ;; TODO re-enable
;(setv fun0 (fnp [p] p)) ;; TODO re-enable
;(assert (= ".places/fnp/d51dcf65284d327c40190ea30b5569f6-f050df7693e362c5bc36dc95ea6345b4" (fun0 #^ 4)))
;(print (code-to-str (macroexpand '(fnp [x] 4))))

#_(if False;True
  (do
    (defnp fun1 [x] (inc x))
    (defnp fun2 [y] (* 2 #$ (fun1 #^ y)))
    (assert (= 2 #$ (fun1 #^ 1)))
    (assert (= 14 #$ (fun2 #^ 6)))

    #_"test fnf"
    (defnf folder1 [x]
      (import pathlib [Path])
      (.touch (Path (+ targ "empty"))))
    (assert (= ["empty"] (os.listdir (folder1 #^ 3))))
    ))
